<?php

namespace Drupal\user_request\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Response Type entity.
 */
interface ResponseTypeInterface extends ConfigEntityInterface {

}
